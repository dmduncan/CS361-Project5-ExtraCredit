import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.util.Scanner;

public class Passwords {
//heyoososo	
	public static void main (String[] args) throws IOException{
		int[] COUNTS = new int[26];
		int[] STARTERS = new int[26];
		int[][][] followers = new int [26][26][26];
		
		String test2 = args[0];
		File text = new File(test2);
		//Scanner sc = new Scanner(text);
		int n = Integer.parseInt(args[1]);
		int k = Integer.parseInt(args[2]);
		
		InputStream in = new FileInputStream(text);
        @SuppressWarnings("resource")
		Reader reader = new InputStreamReader(in);
        
        char ch = 0;
        boolean space = true;
	
		int a;
		int b;
		int c;
		@SuppressWarnings("resource")
		RandomAccessFile file = new RandomAccessFile((text), "r");
		long index, length;
		length = file.length() - 1; 
		for (index = 0; index < length; index++) {
		    a = reader.read();
			file.seek(index+1);
			b = file.read();
			file.seek(index+2);
			c = file.read();
		    
			if (a >= 65 && a <= 90){
				a += 32;
			}
			if (b >= 65 && b <= 90){
				b += 32;
			}
			if(c >= 65 && c <= 90){
				c += 32;
			}
			if(a == 10 || a == 32){
				space = true;
			}
			
			
//COUNTS IMPLEMENTATION
			if(a >= 97 && a <= 122){
				if(b == 10 || b == 32)
				{}
				else{
				//97 is the lowercase a ascii value
					COUNTS[a-97]++;
				}
			}
			
//STARTERS IMPLEMENTATION
			if(a >= 97 && a <= 122 && space){
				//97 is the lowercase a ascii value
				STARTERS[a-97]++;
				space = false;
			}
	
//followers table			
			if(a >= 97 && a <= 122)
			{
				if((b >= 97 && b <= 122))
				{
					if(c >= 97 && c <= 122)
					{
						followers[a - 97][b - 97][c - 97]++;
					}
				}
			}
		}
		
		int total = 0;
		for(int i = 0; i < 26; i++){
			ch = (char)(i + 97);
			total+=STARTERS[i];
		}
		
		int num = 0;
		char ch2 = 0;
		boolean first = true;
		int nextLetter = 0;
		int secondLetter = 0;
		int thirdLetter = 0;
		
		//number of passwords wanted
		System.out.println("Passwords are: ");
		for(int i = 0; i < n; i++)
		{
			int firstLetter = 0;
			first = true;
			num = (int) (Math.random() * (total+1 - 0)) + 0;
			int leng = 0;
			//how long the user wants the password
			for(int f = 0; f < k; f++)
			{
				A: for(int j = 0; j < 26; j++)
				{
					if(leng == k)
						break A;
					firstLetter += STARTERS[j];
					if((firstLetter >= num) && first)
					{
						leng++;
						ch = (char)(j+97);
						nextLetter = j;
						first = false;
						System.out.print(ch);
					}
					ch = (char)(nextLetter+97);
				
					if(first == false)
					{	
						//second letter
						secondLetter = 0;
						int rowSum = 0;
						for(int z = 0; z < 26; z++)
						{
							rowSum+=followers[nextLetter][z][0];
						}
						num = (int) (Math.random() * (rowSum +1 - 0)) + 0;
					
						for(int l = 0; l < 26; l++)
						{
							secondLetter += followers[nextLetter][l][0];
						
							if(secondLetter >= num)
							{
								ch2 = (char)(l+97);
								System.out.print(ch2);
								leng++;
								if(leng == k)
									break A;

								thirdLetter = 0;
								int threeDSum = 0;
								for(int q = 0; q < 26; q++)
								{
									threeDSum += followers[nextLetter][l][q];
								}
								num = (int) (Math.random() * (threeDSum +1 - 0)) + 0;
							
								for(int w = 0; w < 26; w++)
								{
									thirdLetter += followers[nextLetter][l][w];
								
									if(thirdLetter >= num)
									{
										ch2 = (char)(w+97);
										System.out.print(ch2);
										nextLetter = w;
										leng++;
										if(leng == k)
											break A;
										break;
									}
								}
							}
						}
					}
				}
			}
			System.out.println();
		}
		System.out.println();
	}
}
