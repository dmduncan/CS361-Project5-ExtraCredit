UTEID: eab2982; dmd2479; dic223;
FIRSTNAME: Eddie; Daniel; Dylan;
LASTNAME: Babbe; Duncan; Inglis;
CSACCOUNT: dmduncan; babbe99; cinglis;
EMAIL: daniel.duncan@utexas.edu; babbe2012@utexas.edu; dylanmcquade@yahoo.com;

Note: Professor Young has given us permission to work as a team of 3. You can confirm with him if needed.

[Program 5] [Description]
There was really only a need to make 1 file for this assignment, which was Passwords.java. There were 3 arrays used in this program. 2 1D arrays to keep track of the number of occurrences of a letter in the text and then another for the number of occurrences of a letter at the beginning of a word in the text. Then I made a 2D array to record the amount of times a specific letter follows a given letter. With that, when computing random passwords you randomly generate a random number between 0 and the total number of counts of letters at the beginning of a word. Then with that number go through the 1D array called starters adding up each consecutive count until you are equal or greater than the random number, whatever index you are now on is the letter you start with. Once you have the letter to start with you go to that row in the 2D follows array and compute another random number between 0 and the total counts of the row. Then iterate down it and whenever you get the same number or greater than the random computed number that it the next letter following. Then you make that letter the 1st letter and repeat the process for however long you want the password to be. 

[Extra Credit]
I did the first part of the extra credit, which was I took into account the third-order entropy of the english language. I did the same process as the original assignment just I now use a 3D array instead of a 2D array for the follows table. So in the assignment the follows table looks at the first letter and then computes the second letter to follow it. Now my follows table computes the second letter to follow the first and then based on the first and second letters it computes the third letter to follow those two. It does this each iteration and after every iteration the second letter becomes the first letter and gets recomputed for however long the user specifies.  

[Finish] We have completely finished the assignment.

[Source of Reference File]
The text file used for this one was a lot bigger than the first used in the regular project just because if I am putting things in a 3D array I need much more text to be more accurate or the 3rd letter will always be the same few letters. It is part of the CIA world Fact-book and I took it from http://www.gutenberg.org/cache/epub/35830/pg35830.txt.

[[Test Cases]
[Input of test 1]
java Passwords text 15 9

[Output of test 1]

Passwords are: 
stiumvrwa
crossteum
sediajoko
flambnfor
plamandca
terempoqu
ciajakill
azhigrite
trithuuve
abonslamb
frotautvi
reslamand
plamenthe
heyeafrge
mapqirpsu

[Input of test 2]
java Passwords text 10 10

[Output of test 2]

Passwords are: 
thencoulam
ideenflgeh
crastteumv
mbellechei
tryeafigri
vistiyazap
penintausv
clummnoorp
aniniciman
themsnaorp

[Input of test 3]
java Passwords text 20 20

[Output of test 3]

Passwords are: 
fritssetturvewaxcypz
trithurvewlxayszoteu
frashtludviwaxeyrzac
wlembrisocondolpequr
plemanndaelfrgahaint
oteurviwaxcypzplyeaf
tryelgaheirjpkblembn
egahiitjekhlymenevew
ikilemplemandivilitr
eptqaresthetaugvawax
nteurviwaxiyazachwox
artryeaffghhailjukrl
tryeaffghhkivjakillc
blinaospuquroteurviw
vinuavawaxiyezurbset
ifigrhaimjakilimandi
themanxhairtserowwix
cifjokrldmingronocad
iviwaxeyizergsateumv
omplembrasttautvlwax

[Input of test 4]
java Passwords text 15 40

[Output of test 4

Passwords are: 
thernmenviclurasothuavywoxyyazaryergehoi
civirshtsusvewextypzzimmenttiumvvwaxoyez
chillthuivawaxiyazaminthemulamandintrmen
ovewhxayszcrislthutviwaxeyezurasetwutviw
wheiglemevewixeyezicadwheinrysiteurvewax
airrosptiumvywoxiyazalkrastlamentryeafeg
agembnaoappqaryeafdgeheigjukrldminfempaq
wedintausvawaxiyazembrisethutviwaxcyczeb
thembnforpoqurotsucvtwextypzplembnforpoq
aticemplamentrasitsecfagrmentrotouplompe
chainpaqsrasthentrasetwunviwaxayozavewix
ctiumvywoxiyazaricssatighheizjakslombnoo
ftiusvawaxhydzkhaidwaxiyezurastteumvawax
chrminginmerityeafrgehoinjektlimfnaospeq
econaospiqirisplomplimstrepecfagelfresct
permandompaqurasstaunviwaxiyazavewaxeyoz
owixeyozazaintautviwaxiyazuroteurvowexpy
citjekalyeafagehrissitivewsxmyazazamengr
ndegrisnionuavywhxayszarizotautviwaxcypz
inomperompaqirtsittuavewaxiyuzazinfrasst
ecinglimbnpowhaidjakalstrgehoiljukrldmin
plembnaonpaquroteumvrwixeyazavawaxiyazal
flamstautviwaxiyazalitulamandcadonaospeq
cesteunviwaxeyozonaorpoqurasiteurvewixey
yeaffghheigjekelembnporpoqurasttrugvawex
mprthumvrwaxiyazomonaoupsqurcsttuurvewax
civinuavawaxeyszembrosttounviwaxiyazicad
alembnpoppiquruprquresthembnfoutiumvvwax
oneorpoqurbsetaugvawaxiyazicadaerfagrhii
brcstteumvywoxiyazhmencompaqurasstrusvew
whommpaqtryeafggiheirjakilimenthemeteumv
aminaouppqartiotourrosetrunvewaxiyazadae
wheiglimanillwaxwyazarideenflgihoinjukrl
ambnionumvrwaxiyazazayszkrosthserolpiqur
orliclumbnaosplqurthemorthengrestryeafrg
anitiumvawfxayezimanderesumvawaxiyazzeri
pirjakhlymbnpolvawixeybzazatedulamendomp
gretrusvewaxiyyzargulviwaxiyazatitsurviw
tryeafrgohiipjnkslamentryeaffghhaintausv
ciajokslamandinmendomprqirtsitausviwaxay
